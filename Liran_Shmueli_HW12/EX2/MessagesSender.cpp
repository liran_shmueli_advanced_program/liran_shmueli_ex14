#include "MessagesSender.h"

MessagesSender::MessagesSender()
{
	this->_finish = false;
}

MessagesSender::~MessagesSender()
{

}

int MessagesSender::Menu() //menu printing and choice getting
{
	int choice = 0;
	system("cls");
	cout << "1.Sing in" << endl;
	cout << "2.Sing out" << endl;
	cout << "3.Connected Users" << endl;
	cout << "4.Exit" << endl;
	cout << "please enter your choice:";
	cin >> choice;
	return(choice);
}

void MessagesSender::Sing_in() //singing in a new user
{
	string name;
	cout << "Please enter name to sing in:";
	cin >> name;
	if (!(Search_User(name)))
	{
		//fixed here:
		std::unique_lock<std::mutex> lck(this->_Users_Mtx);
		this->_Users.push_back(name);
		lck.unlock();
		//fix end!
		cout << "" << endl;
		cout << "Successfully sign in!" << endl;
		system("PAUSE");
	}
	else
	{
		cout << "this name is already exist!" << endl;
		system("PAUSE");
	}
}

void MessagesSender::Sing_out() //singing out an exist user
{
	string name;
	std::list<string>::iterator it;
	cout << "Please enter name to sing out:";
	cin >> name;
	if ((Search_User(name)))
	{
		//fixed here:
		std::unique_lock<std::mutex> lck(this->_Users_Mtx);
		for (it = this->_Users.begin(); it != this->_Users.end(); it++)
		{
			if (name.compare(*it) == 0)
			{
				this->_Users.erase(it);
				break;
			}
		}
		lck.unlock();
		//fix end!
		cout << "Successfully sign out!" << endl;
	}
	else
	{
		cout << "this user dose not exist please try again!" << endl;
	}
	system("PAUSE");
}

void MessagesSender::Connected_Users() //prints the list of connected users
{
	int i = 0;
	std::list<string>::iterator it;
	cout << "The users connected are:" << endl;
	i = 1;
	//fixed here:
	std::unique_lock<std::mutex> lck(this->_Users_Mtx);
	for (it = this->_Users.begin(); it != this->_Users.end(); it++)
	{
		cout << i << ". " << *it <<endl;
		i++;
	}
	lck.unlock();
	//fix end!
	system("PAUSE");
}

bool MessagesSender::Search_User(string name) //searches a spesific user in the list and returns true if he exist or false if not
{
	bool check=false;
	std::list<string>::iterator it;
	//fixed here:
	std::unique_lock<std::mutex> lck(this->_Users_Mtx);
	for (it = this->_Users.begin(); it != this->_Users.end(); it++)
	{
		if (name.compare(*it) == 0)
		{
			check = true;
		}
	}
	lck.unlock();
	//fix end!
	return(check);
}

void MessagesSender::read_every_minute() //thread function reads from data.txt file
{
	ifstream curr_file;
	ofstream curr_file1;
	string line;
	//fixed here:
	bool Data_Exists = false;	
	while(!(this->_finish))
	{
		Sleep(60000);
		curr_file.open("data.txt");
		while (getline(curr_file, line,'\n'))
		{
			std::unique_lock<std::mutex> lck(this->_Messages_Mtx);
			this->_Messages.push(line);
			lck.unlock();
			Data_Exists = true;
		}
		if (Data_Exists)
		{
			this->_Cv_Messages.notify_all();
		}
		curr_file.close();
		//clean file:
		curr_file1.open("data.txt", std::ofstream::out | std::ofstream::trunc);
		curr_file1.close();
	}
	//fix end!
}

void MessagesSender::write_messages() //thread function write into output.txt file
{
	int len = 0;
	int i = 0;
	ofstream curr_file;
	string line;
	string name;
	string attachment1 = ":";
	string attachment2 = "\n";
	string output_line;
	std::list<string>::iterator it;
	curr_file.open("output.txt");
	//fixed here:
	std::unique_lock<std::mutex> Users_lck(this->_Users_Mtx, std::defer_lock);
	while (!(this->_finish))
	{
		std::unique_lock<std::mutex> Messages_lck(this->_Messages_Mtx);
		if (this->_Messages.empty())
		{
			this->_Cv_Messages.wait(Messages_lck);
		}
		Messages_lck.unlock();

		Messages_lck.lock();
		while (!(this->_Messages.empty()))
		{
			line = this->_Messages.front();
			this->_Messages.pop();
			Messages_lck.unlock();
			std::list<string>::iterator it;
			Users_lck.lock();
			for (it = this->_Users.begin(); it != this->_Users.end(); it++)
			{
				name = *it;
				output_line = name + attachment1 + line + attachment2;
				curr_file << output_line;
			}
			Users_lck.unlock();
			Messages_lck.lock();
		} 
		Messages_lck.unlock();
	}
	curr_file.close();
	//fix end!
}

void MessagesSender::exit_process() //finish the thread proccesses
{
	this->_finish = true;
}
