#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <ctime>
#include <mutex> //threads syncing
#include <list> //added libarys
#include <queue>
#include <Windows.h>
#include <cstdio>
#include <condition_variable>

using namespace std;

class MessagesSender
{

public:
	MessagesSender(); //c'tor and d'tor
	~MessagesSender();

	int Menu(); //menu functions
	void Sing_in();
	void Sing_out();
	void Connected_Users();
	bool Search_User(string name);

	void read_every_minute(); //files proccessing functions
	void write_messages();
	void exit_process();

private:
	list<string> _Users; //shared resources
	queue<string> _Messages;
	bool _finish; //finish proccess bool
	//fixed here:
	std::mutex _Users_Mtx; //locks the list _Users
	std::mutex _Messages_Mtx; //locks the queue _Messages
	std::condition_variable _Cv_Messages;
	//fix end!
};

