#include "MessagesSender.h"

int main()
{
	int choice = 0; //veriables
	MessagesSender MS;

	std::thread read_thread(&MessagesSender::read_every_minute,&MS); //thereads calling
	std::thread write_thread(&MessagesSender::write_messages,&MS);

	read_thread.detach(); //threads detaching
	write_thread.detach();

	choice = MS.Menu(); //menu execution
	while (choice != 4) //while not option 4: exit
	{
		switch (choice)
		{
			case 1: //option 1:sing in
				MS.Sing_in();
				break;
			case 2: //option 2:sing out
				MS.Sing_out();
				break;
			case 3: //option 3: print connected users
				MS.Connected_Users();
				break;
			default: //default option
				cout << "this option dose not exist! please enter again!" << endl;
				system("PAUSE");
				break;
		}
		choice = MS.Menu();
	}

	MS.exit_process(); //finish threads processes
	system("PAUSE"); 
	return(0);
}
