#include "threads.h"

std::mutex mtx; //global mtx

//PART A
void I_Love_Threads()
{
	cout << "I Love Threads!" << endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	//thread running
	t1.join();
}

//PART B
void printVector(vector<int> primes)
{
	std::vector<int>::iterator it;
	cout << "the vector values are:" << endl;
	for (it = primes.begin(); it < primes.end(); it++)
	{
		cout << *it << endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = 0;
	int x = 0;
	bool check = true;
	for (i = begin; i <= end; i++)
	{
		check = true;
		for (x = 2; x <= i / 2; ++x)
		{
			if (i % x == 0)
			{
				check = false;
				break;
			}
		}
		if (check)
		{
			primes.push_back(i);
		}
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	std::clock_t start;
	std::clock_t finish;
	double duration=0.0;
	double duration2 = 0.0;
	int minutes = 0;
	start = std::clock();
	std::thread t1(getPrimes,begin,end,ref(primes));
	//thread running
	t1.join();
	//measure time:
	finish= std::clock();
	duration = (finish - start) / (double)CLOCKS_PER_SEC;
	duration2 = duration;
	while (duration >= 60.0)
	{
		duration = duration - 60.0;
		minutes++;
	}
	cout << "total time: " << minutes << " minutes and "<< duration << " seconds"<< endl;
	cout << "total time in seconds: " << duration2 << endl;
	return(primes);
}

//PART D
void writePrimesToFile(int begin, int end, ofstream& file)
{
	int i = 0;
	int x = 0;
	bool check = true;
	for (i = begin; i <= end; i++)
	{
		check = true;
		for (x = 2; x <= i / 2; ++x)
		{
			if (i % x == 0)
			{
				check = false;
				break;
			}
		}
		if (check)
		{
			mtx.lock();
			file << i << endl;
			mtx.unlock();
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int i = 0;
	int range = 0;
	std::clock_t start;
	std::clock_t finish;
	double duration = 0.0;
	double duration2 = 0.0;
	int minutes = 0;
	std::thread* arr = new std::thread[N];
	ofstream file;
	start = std::clock();
	file.open(filePath);
	range = (end - begin) / N;
	for (i = 0; i < N-1; i++)
	{
		arr[i] = std::thread(writePrimesToFile,begin,range+begin,ref(file));
		begin =begin+ range+1;
	}
	arr[N - 1] = std::thread(writePrimesToFile, begin,end, ref(file));
	for (i = 0; i < N; i++)
	{
		arr[i].join();
	}
	file.close();
	finish = std::clock();
	duration = (finish - start) / (double)CLOCKS_PER_SEC;
	duration2 = duration;
	while (duration >= 60.0)
	{
		duration = duration - 60.0;
		minutes++;
	}
	cout << "total time: " << minutes << " minutes and " << duration << " seconds" << endl;
	cout << "total time in seconds: " << duration2 << endl;
	delete[] arr;
}
