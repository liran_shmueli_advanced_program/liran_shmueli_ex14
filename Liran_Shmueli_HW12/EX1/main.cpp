#include "threads.h"

int main()
{
	vector<int> primes1;
	vector<int> primes2;
	vector<int> primes3;
	//PART A
	call_I_Love_Threads();
	//PART C
	primes1 = callGetPrimes(0,1000);
	printVector(primes1);
	primes2 = callGetPrimes(0, 100000);
	printVector(primes2);
	primes3 = callGetPrimes(0, 1000000);
	printVector(primes3); 
	//PART E
	callWritePrimesMultipleThreads(0, 1000, "file.txt", 2);
	callWritePrimesMultipleThreads(0, 100000, "file2.txt", 3);
	callWritePrimesMultipleThreads(0, 1000000, "file3.txt", 4);
	system("pause");
	return 0;
}
